fn main(ints: Vec<usize>) -> bool {
    if ints[0] == 0 {
        return false;
    }
    let goal = ints.len() - 1;
    let mut i = 0;
    loop {
        let cur = ints[i];
        if i + cur == goal {
            return true;
        }
        if i + cur > ints.len() || cur == 0 {
            return false;
        }
        i = cur + i;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_is_true() {
        assert_eq!(main(vec![2, 0, 1, 0]), true);
        assert_eq!(main(vec![2, 0, 2, 0, 1, 1, 0]), true);
    }

    #[test]
    fn test_is_false() {
        assert_eq!(main(vec![1, 1, 0, 1]), false);
    }
}
